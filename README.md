## Author

```
name: Oleksandr
last name: Horiainov
e-mail: dn090784gap@gmail.com
```

## Task


#### Создай web-приложение для работы с информацией о людях.

##### HTML+CSS

- нужно сделать страницу, на которой будет таблица с информацией о людях;
- таблица должна иметь 3 следующие колонки: First name, Second name, E-mail.

##### PHP+MySQL

- нужно предоставить возможность добавления, удаления и изменения записей таблицы;
- данные о людях должны сохраняться в базе данных.

##### Javascript

- нужно сделать проверку полей ввода перед отправкой на сервер;
- все поля обязательны для заполнения! Поле E-mail должно содержать валидный адрес электронной почты.

##### Ajax

- нужно сделать, чтобы добавление, изменение и удаление записей таблицы производились без перезагрузки страницы;
- все действия должны производиться на странице с таблицей.

## Instalation

#### Used technologies

```
nginx/1.14.0

PHP 7.2.19

10.1.29-MariaDB-6

jquery/1.12.4

bootstrap/3.3.7
```

- Run commands

```
1. git clone https://gapdn@bitbucket.org/gapdn/test_webinse-laravel.git
2. composer install
3. cp .env.example .env
4. php artisan key:generate
5. php artisan migrate
```

- Update '.env' file with your DB settings
```
...
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_db_name
DB_USERNAME=your_user_name
DB_PASSWORD=your_user_password
...
```
- Configure Nginx settings

- Enjoy ))