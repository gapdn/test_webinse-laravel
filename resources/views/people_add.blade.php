<div class="modal fade" id="addPeopleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="validate" id="frmAddPeople">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Add New
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="add-error-bag">
                        <ul id="add-people-errors">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label>
                            First name
                        </label>
                        <input class="form-control" id="first_name" name="first_name" required="" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label>
                            Second name
                        </label>
                        <input class="form-control" id="second_name" name="second_name" required="" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label>
                            Email
                        </label>
                        <input class="form-control" id="email" name="email" required="" type="email">
                        </input>
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-default" data-dismiss="modal" type="button" value="Cancel">
                    <button class="btn btn-info" id="btn-add" type="button" value="add">
                        Add New Person
                    </button>
                    </input>
                </div>
            </form>
        </div>
    </div>
</div>
