<div class="modal fade" id="editPeopleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="validate" id="frmEditPeople">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit
                    </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-people-errors">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="first_name">
                            First name
                        </label>
                        <input class="form-control" id="first_name" name="first_name" required="" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="second_name">
                            Second name
                        </label>
                        <input class="form-control" id="second_name" name="second_name" required="" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="email">
                            Email
                        </label>
                        <input class="form-control" id="email" name="email" required="" type="email">
                        </input>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="people_id" name="people_id" type="hidden" value="0">
                    <input class="btn btn-default" data-dismiss="modal" type="button" value="Cancel">
                    <button class="btn btn-info" id="btn-edit" type="button" value="add">
                        Update Person
                    </button>
                    </input>
                    </input>
                </div>
            </form>
        </div>
    </div>
</div>