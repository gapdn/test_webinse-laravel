$(document).ready(function() {

    $("input").change(function (){
        $(".validate").validate({
            ignore: ":hidden",
            rules: {
                first_name: {
                    required: true,
                    minlength: 3,
                    maxlength: 42
                },
                second_name: {
                    required: true,
                    minlength: 3,
                    maxlength: 42
                }
            }
        });
    });


    $("#btn-add").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/',
            data: {
                first_name: $("#frmAddPeople input[name=first_name]").val(),
                second_name: $("#frmAddPeople input[name=second_name]").val(),
                email: $("#frmAddPeople input[name=email]").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmAddPeople').trigger("reset");
                $("#frmAddPeople .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-people-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#add-people-errors').append('<li>' + value + '</li>');
                });
                $("#add-error-bag").show();
            }
        });
    });

    $("#btn-edit").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'PUT',
            url: '/' + $("#frmEditPeople input[name=people_id]").val(),
            data: {
                first_name: $("#frmEditPeople input[name=first_name]").val(),
                second_name: $("#frmEditPeople input[name=second_name]").val(),
                email: $("#frmEditPeople input[name=email]").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#frmEditPeople').trigger("reset");
                $("#frmEditPeople .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#edit-people-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#edit-people-errors').append('<li>' + value + '</li>');
                });
                $("#edit-error-bag").show();
            }
        });
    });
    $("#btn-delete").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/' + $("#frmDeletePeople input[name=people_id]").val(),
            dataType: 'json',
            success: function(data) {
                $("#frmDeletePeople .close").click();
                window.location.reload();
            },
            error: function(data) {
                console.log(data);
            }
        });
    });


});

function addPeopleForm() {
    $(document).ready(function() {
        $("#add-error-bag").hide();
        $('#addPeopleModal').modal('show');
    });
}

function editPeopleForm(people_id) {
    $.ajax({
        type: 'GET',
        url: '/' + people_id,
        success: function(data) {
            $("#edit-error-bag").hide();
            $("#frmEditPeople input[name=first_name]").val(data.people.first_name);
            $("#frmEditPeople input[name=second_name]").val(data.people.second_name);
            $("#frmEditPeople input[name=email]").val(data.people.email);
            $("#frmEditPeople input[name=people_id]").val(data.people.id);
            $('#editPeopleModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function deletePeopleForm(people_id) {
    $.ajax({
        type: 'GET',
        url: '/' + people_id,
        success: function(data) {
            $("#frmDeletePeople #delete-title").html("Delete Person (" + data.people.first_name + " " + data.people.second_name + ")?");
            $("#frmDeletePeople input[name=people_id]").val(data.people.id);
            $('#deletePeopleModal').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}