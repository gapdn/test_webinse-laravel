<?php


namespace App\Http\Controllers;

use App\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PeopleController extends Controller
{
    public function index()
    {
        $peoples = People::orderBy('id', 'desc')->get();

        return view('people', ['peoples' => $peoples]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->input(), array(
            'first_name' => 'required|string|min:3|max:42',
            'second_name' => 'required|string|min:3|max:42',
            'email' => 'required|email|max:42',
        ));

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }

        $people = People::create($request->all());

        return response()->json([
            'error' => false,
            'people'  => $people,
        ], 200);
    }

    public function show($id)
    {
        $people = People::find($id);

        return response()->json([
            'error' => false,
            'people'  => $people,
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->input(), array(
            'first_name' => 'required|string|min:3|max:42',
            'second_name' => 'required|string|min:3|max:42',
            'email' => 'required|email|max:42',
        ));

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }

        $people = People::find($id);

        $people->first_name =  $request->input('first_name');
        $people->second_name = $request->input('second_name');
        $people->email = $request->input('email');

        $people->save();

        return response()->json([
            'error' => false,
            'people'  => $people,
        ], 200);
    }

    public function destroy($id)
    {
        $people = People::destroy($id);

        return response()->json([
            'error' => false,
            'people'  => $people,
        ], 200);
    }
}