<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PeopleController@index')->name('index');
Route::get('/{id}', 'PeopleController@show')->name('show');
Route::post('/', 'PeopleController@store')->name('store');
Route::put('/{id}', 'PeopleController@update')->name('update');
Route::delete('/{id}', 'PeopleController@destroy')->name('destroy');
